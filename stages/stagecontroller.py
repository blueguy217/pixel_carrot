from gamedata import GameData

from stages.welcome import welcome_screen
from stages.gameloop import game_loop
from stages.menu import menu
from stages.worldmap import worldmap

class StageController:
    def __init__(self, game: GameData):
        self.game = game
        self.game.stage_ctrl = self

        self.next_stage = "welcome"

        self.stages = ['welcome',
                       'menu',
                       'world_map',
                       'game_loop']

        # Fullscreen on
        # self.game.fullscreen = True
        # self.game.screen = pygame.display.set_mode(self.game.MONITOR_SIZE, pygame.FULLSCREEN)

    def run_next_stage(self):

        if self.next_stage == "welcome":
            welcome_screen(self.game)
            self.next_stage = "game_loop"

        elif self.next_stage == "menu":
            menu(self.game)
            self.next_stage = "world_map"

        elif self.next_stage == "world_map":
            worldmap(self.game)
            self.next_stage = "game_loop"

        elif self.next_stage == "game_loop":
            game_loop(self.game)
