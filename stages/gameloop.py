import sys
import pygame
from pygame.locals import *

from constants import *
from gamedata import GameData


def collision_test(rect, tiled_objects):
    hit_list = []
    for tile in tiled_objects:
        collision_rect = tile.get_collision_rect()
        if not collision_rect:
            continue
        if rect.colliderect(collision_rect):
            hit_list.append(tile)
    return hit_list

def move(player, tiled_objects):
    rect = player.player_rect
    movement = player.movement
    player.on_ramp = False  # reset to default
    collision_types = {'top': False, 'bottom': False, 'right': False, 'left': False}

    rect.x += movement[0]
    hit_list = collision_test(rect, tiled_objects)
    for tile in hit_list:
        if movement[0] > 0:
            collision_types['right'] = True
            tile.action_on_collision(player, collision_types)
        elif movement[0] < 0:
            collision_types['left'] = True
            tile.action_on_collision(player, collision_types)

    collision_types = {'top': False, 'bottom': False, 'right': False, 'left': False}
    rect.y += movement[1]
    hit_list = collision_test(rect, tiled_objects)
    for tile in hit_list:
        if movement[1] > 0:
            collision_types['bottom'] = True
            tile.action_on_collision(player, collision_types)
        elif movement[1] < 0:
            collision_types['top'] = True
            tile.action_on_collision(player, collision_types)

    return rect, collision_types


def game_loop(game: GameData):

    while True:
        # background
        game.display.fill(BACKGROUND)

        # render background
        game.background.render(game.display, game.camera.scroll)

        # player movement
        game.player.update_movement()

        # COLLISIONS
        game.player.player_rect, collisions = move(game.player, game.map.tiled_objects)
        if collisions['bottom']:
            game.player.air_timer = 0
        else:
            game.player.air_timer += 1

        # MAP update
        for i in game.map.tiled_objects:
            i.render(game.display, game.camera.scroll)
            # i.draw_collision_rect(game.display, scroll)

        # camera movement
        game.camera.update()

        # BLIT PLAYER
        game.player.blit(game.display, game.camera.scroll)

        for p in game.particles:
            p.render()

        # DRAW GUI
        game.gui.render(game.display)

        # EVENTS ===================================================

        # mouse collision
        # mx, my = pygame.mouse.get_pos() # mouse position
        # if some_rect.collidepoint((mx, my)):
        #     if click:
        #         pass
        click = False

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            # if event.type == VIDEORESIZE:
            #     if not game.fullscreen:
            #         game.screen = pygame.display.set_mode((event.w, event.h), pygame.RESIZABLE)
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == K_f:
                    game.fullscreen = not game.fullscreen
                    if game.fullscreen:
                        game.screen = pygame.display.set_mode(game.MONITOR_SIZE, pygame.FULLSCREEN)
                    else:
                        game.screen = pygame.display.set_mode(WINDOW_SIZE, 0, 32)
                if event.key == K_RIGHT:
                    game.player.moving_right = True
                if event.key == K_LEFT:
                    game.player.moving_left = True
                if event.key == K_SPACE:  # jump
                    if game.player.air_timer < MAX_AIR_TIME:
                        game.player.start_jumping()
            if event.type == KEYUP:
                if event.key == K_RIGHT:
                    game.player.moving_right = False
                if event.key == K_LEFT:
                    game.player.moving_left = False
                if event.key == K_SPACE:  # stop jumping
                    game.player.jumping = False

            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        # update fps
        fps = str(int(game.clock.get_fps()))
        fps_text = game.font.render(fps, 1, pygame.Color((255,100,100)))
        game.display.blit(fps_text, (DISPLAY_SIZE[0] - 20,0))

        # UPDATE WINDOW ============================================
        surf = None
        if game.fullscreen:
            surf = pygame.transform.scale(game.display, game.MONITOR_SIZE)
        else:
            surf = pygame.transform.scale(game.display, WINDOW_SIZE)
        game.screen.blit(surf, (0, 0))
        pygame.display.update()
        game.clock.tick(60)