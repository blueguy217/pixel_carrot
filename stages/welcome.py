import sys
import pygame
from pygame.locals import *

from constants import *
from gamedata import GameData


def zoom_out(display, step):
    position = (80, 100)
    radius = 275 - step
    if radius <= 0:
        radius = 0
    for i in range(radius, 300):
        pygame.draw.circle(display, (0, 0, 0), position, radius=i, width=2)
    if step > 300:
        return True
    return False

def zoom_in(display, step):
    position = (80, 100)
    for i in range(step, 300):
        pygame.draw.circle(display, (0, 0, 0), position, radius=i, width=2)
    if step > 300:
        return True
    return False

def draw_background(display, bck_move):
    a = 10
    color = (65, 146, 195)

    for i in range(20):
        for j in range(15):
            pos = (-5*a + i*2*a + int(bck_move[0]), -3*a + j*2*a + int(bck_move[1]))
            points = []
            points.append((pos[0], pos[1]))
            points.append((pos[0]+a, pos[1]+a))
            points.append((pos[0], pos[1]+2*a))
            points.append((pos[0]-a, pos[1]+a))
            pygame.draw.polygon(display, color, points)


opacity = 255
fading = True
def blink_img(game: GameData, img, position):
    global opacity, fading
    if opacity >= 255:
        fading = True
    elif opacity <= 0:
        fading = False

    if fading:
        opacity -= 5
    else:
        opacity += 10
    img.set_alpha(opacity)
    game.display.blit(img, position)

def welcome_screen(game: GameData):

    show = True

    text_img = pygame.image.load('Data/Img/title_text.png')
    rooster_img = pygame.image.load('Data/Img/title_rooster.png')
    start_txt = pygame.image.load('Data/Img/start_txt.png')

    step = 0
    run_animation = {'in': True, 'out': False}
    finished_animation = False

    bck_move = [0, 0]
    bck_direction = {'tr': False, 'tl': False, 'br': True, 'bl': False}

    while show:

        game.display.fill((48, 81, 130))

        draw_background(game.display, bck_move)

        mv = 0.3    # background movement speed
        if bck_direction['tr']:
            bck_move[0] += mv
            bck_move[1] -= mv
            if bck_move[1] <= 0:
                bck_direction['tr'] = False
                bck_direction['tl'] = True
        if bck_direction['tl']:
            bck_move[0] -= mv
            bck_move[1] -= mv
            if bck_move[1] <= -30:
                bck_direction['tl'] = False
                bck_direction['bl'] = True
        if bck_direction['br']:
            bck_move[0] += mv
            bck_move[1] += mv
            if bck_move[0] >= 30:
                bck_direction['br'] = False
                bck_direction['tr'] = True
        if bck_direction['bl']:
            bck_move[0] -= mv
            bck_move[1] += mv
            if bck_move[1] >= 0:
                bck_direction['bl'] = False
                bck_direction['br'] = True

        game.display.blit(text_img, (0, 0))
        game.display.blit(rooster_img, (0, 0))

        if run_animation['in']:
            step += 2
            finished_animation = zoom_in(game.display, step)
            if finished_animation:
                run_animation['in'] = False
        elif run_animation['out']:
            step += 2
            finished_animation = zoom_out(game.display, step)
            if finished_animation:
                show = False
        else:
            blink_img(game, start_txt, (127, 144))

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()

                if not run_animation['in'] and not run_animation['out']:
                    run_animation['out'] = True
                    finished_animation = False
                    step = 0

        # Update window
        surf = None
        if game.fullscreen:
            surf = pygame.transform.scale(game.display, game.MONITOR_SIZE)
        else:
            surf = pygame.transform.scale(game.display, WINDOW_SIZE)
        game.screen.blit(surf, (0, 0))
        pygame.display.update()
        game.clock.tick(60)