import pygame
from pygame.locals import *

from constants import *
from gamedata import GameData
from camera import Camera
from map import Map
from tiledobject import TiledObject
from jumpers import Jumpers
from platforms import Platforms
from water import Water
from carrots import Carrots
from parallax import Parallax
from player import Player
from gui import Gui

from stages.stagecontroller import StageController

pygame.init()
pygame.display.set_caption('Carrot')

MONITOR_SIZE = [pygame.display.Info().current_w, pygame.display.Info().current_h]
print("Monitor size: " + str(MONITOR_SIZE))


if __name__ == '__main__':

    game = GameData(MONITOR_SIZE)

    game.clock = pygame.time.Clock()
    game.camera = Camera(game)
    stage_ctrl = StageController(game)

    game.map = Map('Data/map.tmx')
    game.player = Player(game.map.player_start_position, game)

    jumpers = Jumpers(game.map.jumpers)
    platforms = Platforms(game, game.map.platforms)
    carrots = Carrots(game.map.carrots)
    water = Water(game.map.water_areas)
    game_objects = [game.map, jumpers, platforms, carrots, water]

    tiled_objects = []
    for i in game_objects:
        tiled_objects += i.get_tiles()
    game.map.tiled_objects = tiled_objects

    game.background = Parallax()
    game.gui = Gui(game.player)

    # THE GAME
    while True:
        stage_ctrl.run_next_stage() # welcome_screen(game)

