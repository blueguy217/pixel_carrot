import pygame

from constants import *
from particles import Particle


class GameData:
    def __init__(self, monitor_size):
        self.screen = pygame.display.set_mode(WINDOW_SIZE, 0, 32)
        MONITOR_SIZE = [pygame.display.Info().current_w, pygame.display.Info().current_h]
        # self.screen = pygame.display.set_mode(WINDOW_SIZE, pygame.RESIZABLE)
        self.display = pygame.Surface(DISPLAY_SIZE)

        self.font = pygame.font.SysFont('Arial', 18)

        self.fullscreen = False

        # self.fullscreen = True
        # self.screen = pygame.display.set_mode(MONITOR_SIZE, pygame.FULLSCREEN)

        self.MONITOR_SIZE = monitor_size

        self.clock = None
        self.camera = None
        self.stage_ctrl = None
        self.player = None
        self.map = None
        self.gui = None
        self.background = None

        self.particles = []

