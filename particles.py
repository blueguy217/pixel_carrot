import pygame
import random

# from gamedata import GameData

random.seed(a = 1)

class Particle:
    def __init__(self, game):
        self.game = game
        self.player_rect = game.player.player_rect

        self.counter = 0
        self.max_counter = 100

        game.particles.append(self)

    def render(self):
        self.counter += 1
        if self.counter >= self.max_counter:
            self.destroy()
            return

    def destroy(self):
        self.game.particles.remove(self)


class RunningDust(Particle):
    def __init__(self, game):
        super().__init__(game)

        self.player_direction = 'right' if game.player.moving_right is True else 'left'

        self.size = random.randint(1, 3)
        self.x = self.player_rect.left if game.player.moving_right is True else self.player_rect.right
        self.y = self.player_rect.bottom - self.size

    def render(self):
        super().render()

        scroll = self.game.camera.scroll
        x = self.x - scroll[0]
        y = self.y - scroll[1]

        # change size every some time
        if self.counter % 10 == 0:
            self.size -= 1
            if self.size <= 0:
                self.destroy()
                return

            y -= self.size

            self.y -= random.randint(1, 2)

            if self.player_direction == 'right':
                x -= self.size
                self.x -= random.randint(1, 3)
            elif self.player_direction == 'left':
                x += self.size
                self.x += random.randint(1, 3)

        draw_rect = pygame.Rect(x, y, self.size, self.size)
        pygame.draw.rect(self.game.display, (255, 255, 255), draw_rect)
        frame_rect = pygame.Rect(x-1, y-1, self.size+2, self.size+2)
        pygame.draw.rect(self.game.display, (121, 121, 121), frame_rect, width=1)


class JumpDust(Particle):
    def __init__(self, game):
        super().__init__(game)

        player_middle = int((self.player_rect.right - self.player_rect.left) / 2)
        pos_shift = random.randint(1, 4)
        self.x_l = self.player_rect.left + player_middle - pos_shift
        self.x_r = self.player_rect.right - player_middle + pos_shift
        self.y = self.player_rect.bottom

        self.w = random.randint(2, 4)
        self.h = random.randint(2, 4)

    def render(self):
        super().render()

        scroll = self.game.camera.scroll
        x_l = self.x_l - scroll[0] - self.w
        x_r = self.x_r - scroll[0]
        y = self.y - scroll[1] - self.h

        rect_left = pygame.Rect(x_l, y, self.w, self.h)
        pygame.draw.rect(self.game.display, (255, 255, 255), rect_left)
        frame_rect = pygame.Rect(x_l - 1, y - 1, self.w + 2, self.h + 2)
        pygame.draw.rect(self.game.display, (121, 121, 121), frame_rect, width=1)

        rect_right = pygame.Rect(x_r, y, self.w, self.h)
        pygame.draw.rect(self.game.display, (255, 255, 255), rect_right)
        frame_rect = pygame.Rect(x_r - 1, y - 1, self.w + 2, self.h + 2)
        pygame.draw.rect(self.game.display, (121, 121, 121), frame_rect, width=1)

        if self.counter % 8 == 0:
            self.w -= 1
            self.h -= 1
            self.x_l -= 3
            self.x_r += 3

        if self.w <= 0:
            self.destroy()