import pytmx
import pygame

from tiledobject import TiledObject, Position
from constants import *

class Ramp(TiledObject):
    def __init__(self, pos, tile_rect, type):

        img = None
        if type == 'ramp1_up':
            img = [pygame.image.load('Data/Img/ramp1_up.png')]
        elif type == 'ramp1_down':
            img = [pygame.image.load('Data/Img/ramp1_down.png')]
        elif type == 'ramp2_up':
            img = [pygame.image.load('Data/Img/ramp2_up.png')]
        elif type == 'ramp2_down':
            img = [pygame.image.load('Data/Img/ramp2_down.png')]
        super().__init__(img, pos, tile_rect)

        self.ramp_type = {'ramp1_up': False, 'ramp1_down': False,
                          'ramp2_up': False, 'ramp2_down': False}
        self.ramp_type[type] = True

    def render(self, surface, scroll):
        super().render(surface, scroll)

        # DEBUG: draw rectangle
        # draw_rect = pygame.Rect(self.rect.x - scroll[0], self.rect.y - scroll[1], self.rect.w, self.rect.h)
        # pygame.draw.rect(surface, (255, 0, 0), draw_rect, width=1)

    def action_on_collision(self, player, collision_types):
        # super().action_on_collision(player, collision_types)
        # from super() =========
        if not self.active:
            return
        # ======================

        if player.movement[1] < 0 and player.player_rect.bottom <= self.rect.bottom - 16:
            return

        # secure collision below the ramp
        if collision_types['top']:
            player.player_y_momentum = 0
            player.player_rect.top = self.rect.bottom
            return

        player.on_ramp = True

        if self.ramp_type['ramp1_up']:

            # secure collision below the ramp when going left
            if collision_types['left'] and player.player_rect.left >= self.rect.right - 16 and player.player_rect.top >= self.rect.bottom - 16:
                player.player_rect.left = self.rect.right
                return

            player_middle = player.player_rect.x + int(player.player_rect.w / 2)
            diff_x = int((player_middle - self.rect.left) / 2)

            if collision_types['bottom'] and diff_x < 0:
                # print(f'Error! diff: {diff_x}')
                return

            if diff_x > 16:
                # print(f'Error! diff: {diff_x}')
                return

            pos_y = self.rect.bottom - diff_x - 16
            player.player_rect.bottom = pos_y

        elif self.ramp_type['ramp1_down']:

            # secure collision below the ramp when going right
            if collision_types['right'] and player.player_rect.right >= self.rect.left + 16 and player.player_rect.top >= self.rect.bottom - 16:
                player.player_rect.right = self.rect.left
                return

            player_middle = player.player_rect.x + int(player.player_rect.w / 2)
            diff_x = int((self.rect.right - player_middle) / 2)

            if collision_types['bottom'] and diff_x < 0:
                # print(f'Error! diff: {diff_x}')
                return

            if diff_x > 16:
                # print(f'Error! diff: {diff_x}')
                return

            pos_y = self.rect.bottom - diff_x - 16
            player.player_rect.bottom = pos_y

        elif self.ramp_type['ramp2_up']:

            # secure collision below the ramp when going left
            if collision_types['left'] and player.player_rect.left >= self.rect.right - 8 and player.player_rect.top >= self.rect.bottom - 16:
                player.player_rect.left = self.rect.right
                return

            player_middle = player.player_rect.x + int(player.player_rect.w / 2)
            diff_x = int(player_middle - self.rect.left)

            if collision_types['bottom'] and diff_x < 0:
                # print(f'Error! diff: {diff_x}')
                return

            if diff_x > 16:
                # print(f'Error! diff: {diff_x}')
                return

            pos_y = self.rect.bottom - diff_x - 16
            player.player_rect.bottom = pos_y

        elif self.ramp_type['ramp2_down']:

            # secure collision below the ramp when going right
            if collision_types['right'] and player.player_rect.right >= self.rect.left + 8 and player.player_rect.top >= self.rect.bottom - 16:
                player.player_rect.right = self.rect.left
                return

            player_middle = player.player_rect.x + int(player.player_rect.w / 2)
            diff_x = int(self.rect.right - player_middle)

            if collision_types['bottom'] and diff_x < 0:
                # print(f'Error! diff: {diff_x}')
                return

            if diff_x > 16:
                # print(f'Error! diff: {diff_x}')
                return

            pos_y = self.rect.bottom - diff_x - 16
            player.player_rect.bottom = pos_y


class Tile(TiledObject):
    def __init__(self, img, pos, tile_rect):
        super().__init__(img, pos, tile_rect)

        # print(f'New title: {self.x},{self.y},{self.w},{self.h},{self.img_id}')

class Map:
    def __init__(self, filename):
        tm = pytmx.load_pygame(filename, pixelalpha=False)
        self.width = tm.width * tm.tilewidth
        self.height = tm.height * tm.tileheight
        self.tmxdata = tm

        self.player_start_position = (50,50)

        self.tiles = []
        self.ramps = []
        self.bck_tiles = []

        self.jumpers = []
        self.platforms = []
        self.water_areas = []
        self.carrots = []

        self.tiled_objects = []

        self.scroll = [0, 0]    # hold camera movement

        self.collect_all_tiles()
        self.update_side_collisions()

    def collect_all_tiles(self):
        self.tile_rects = []    # clear
        ti = self.tmxdata.get_tile_image_by_gid
        for layer in self.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledTileLayer):
                if layer.name == 'ground':
                    for x, y, gid, in layer:
                        tile = ti(gid)
                        if tile:
                            tile_rect = pygame.Rect(x * self.tmxdata.tilewidth,
                                                   y * self.tmxdata.tileheight,
                                                   self.tmxdata.tilewidth,
                                                   self.tmxdata.tileheight)
                            pos = Position(tile_rect.x, tile_rect.y)
                            self.tiles.append(Tile([tile], pos, tile_rect))
                if layer.name == 'background':
                    for x, y, gid in layer:
                        tile = ti(gid)
                        if tile:
                            pos = Position(x * self.tmxdata.tilewidth, y * self.tmxdata.tileheight)
                            self.bck_tiles.append(Tile([tile], pos, None))
            elif isinstance(layer, pytmx.TiledObjectGroup):
                if layer.name == 'objects':
                    for i in layer:
                        if i.name == 'player':
                            self.player_start_position = (i.x, i.y)
                        elif i.name == 'jumper':
                            self.jumpers.append((i.x, i.y))
                        elif i.name == 'carrot':
                            self.carrots.append((i.x, i.y))
                        elif i.name == 'platform':
                            self.platforms.append((i.x, i.y, i.width, i.height))
                        elif i.name == 'water':
                            rect = pygame.Rect(i.x, i.y, i.width, i.height)
                            self.water_areas.append(rect)
                        # ramps
                        elif i.name == 'ramp1_up':
                            tile_rect = pygame.Rect(i.x, i.y, 32, 32)
                            self.ramps.append(Ramp(Position(i.x, i.y), tile_rect, i.name))
                        elif i.name == 'ramp1_down':
                            tile_rect = pygame.Rect(i.x, i.y, 32, 32)
                            self.ramps.append(Ramp(Position(i.x, i.y), tile_rect, i.name))
                        elif i.name == 'ramp2_up':
                            tile_rect = pygame.Rect(i.x, i.y, 16, 32)
                            self.ramps.append(Ramp(Position(i.x, i.y), tile_rect, i.name))
                        elif i.name == 'ramp2_down':
                            tile_rect = pygame.Rect(i.x, i.y, 16, 32)
                            self.ramps.append(Ramp(Position(i.x, i.y), tile_rect, i.name))

    def get_tiles(self):
        return self.tiles + self.ramps + self.bck_tiles

    # for ramps it is needed that some tiles will not have side collisions
    def update_side_collisions(self):

        # collect 'ramp1_up' ramps
        ramps_to_check = []
        for ramp in self.ramps:
            if ramp.ramp_type['ramp1_up'] or ramp.ramp_type['ramp2_up']:
                ramps_to_check.append(ramp)

        # iterate over all tiles and check if it's on the right side of ramp
        for t in self.tiles:
            for r in ramps_to_check:
                if r.rect.collidepoint(t.rect.left - 1, t.rect.y):
                    t.has_side_collisions = False

        # collect 'ramp1_down' ramps
        ramps_to_check = []
        for ramp in self.ramps:
            if ramp.ramp_type['ramp1_down'] or ramp.ramp_type['ramp2_down']:
                ramps_to_check.append(ramp)

        # iterate over all tiles and check if it's on the left side of ramp
        for t in self.tiles:
            for r in ramps_to_check:
                if r.rect.collidepoint(t.rect.right + 1, t.rect.y):
                    t.has_side_collisions = False

