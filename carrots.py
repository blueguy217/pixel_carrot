import pygame

from tiledobject import TiledObject, Position

class Carrot(TiledObject):
    def __init__(self, img, rect):
        pos = Position(rect.x, rect.y)
        super().__init__(img, pos, rect, animated=False)

    def action_on_collision(self, player, collision_types):
        player.life_up()
        self.active = False

class Carrots:
    def __init__(self, carrots):
        self.carrot_img = pygame.image.load('Data/Img/carrot.png')
        self.w = self.carrot_img.get_width()
        self.h = self.carrot_img.get_height()
        self.carrots = []
        for c in carrots:
            rect = pygame.Rect(c[0], c[1], self.w, self.h)
            self.carrots.append(Carrot([self.carrot_img], rect))
        self.animation_frame = 0
        self.move = (0, 0)

    def get_tiles(self):
        return self.carrots
