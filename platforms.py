import pygame

from tiledobject import TiledObject, Position


class Platform(TiledObject):
    def __init__(self, game, img, rect, area_rect):
        pos = Position(area_rect.x, area_rect.y)
        super().__init__(img, pos, rect)

        self.min_x = self.pos.x
        self.max_x = self.pos.x + area_rect.w - self.rect.w

        self.going_right = True
        self.current_pos = self.pos.x

        self.last_pos_x = self.rect.x
        self.player_pos_x = 0

        self.player = None
        self.game = game

        self.on_platform = False
        self.diff = 0

    def render(self, surface, scroll):
        # TODO: check if jumper is in range of camera view
        if self.going_right:
            self.current_pos += 0.5
            if self.current_pos >= self.max_x:
                self.going_right = False
        else:
            self.current_pos -= 0.5
            if self.current_pos <= self.min_x:
                self.going_right = True

        if self.player:
            if self.collision_types['bottom']:
                self.on_platform = True
            else:
                if self.player_pos_x == self.player.player_rect.x and self.player.on_ground:
                    self.on_platform = True
                else:
                    self.diff = 0
                    self.on_platform = False

        self.rect.x = int(self.current_pos)

        if self.on_platform:
            self.diff = self.rect.x - self.last_pos_x
            self.player.player_rect.x += self.diff
            self.player_pos_x = self.player.player_rect.x
            self.game.camera.update()

        self.last_pos_x = self.rect.x

        surface.blit(self.img[0], (self.rect.x - scroll[0], self.rect.y - scroll[1]))

    def action_on_collision(self, player, collision_types):
        super().action_on_collision(player, collision_types)
        if not self.player:
            self.player = player

class Platforms:
    def __init__(self, game, platforms):
        self.platforms = []
        self.game = game

        self.img = pygame.image.load('Data/Img/floating_platform.png')
        self.w = self.img.get_width()
        self.h = self.img.get_height()

        for p in platforms:
            collision_rect = pygame.Rect(p[0], p[1], self.w, self.h)
            area_rect = pygame.Rect(p[0], p[1], p[2], p[3])
            self.platforms.append(Platform(self.game, [self.img], collision_rect, area_rect))

    # def render(self, surface, scroll):
    #     for p in self.platforms:
    #         # TODO: check if jumper is in range of camera view
    #         if p.going_right:
    #             p.current_pos += 0.5
    #             if p.current_pos >= p.max_x:
    #                 p.going_right = False
    #         else:
    #             p.current_pos -= 0.5
    #             if p.current_pos <= p.min_x:
    #                 p.going_right = True
    #         p.pos_x = int(p.current_pos)
    #         surface.blit(self.img, (p.pos_x - scroll[0], p.pos_y - scroll[1]))

    # def get_collision_rects(self):
    #     rects = []
    #     for p in self.platforms:
    #         rects.append(p.get_rect())
    #     return rects

    def get_tiles(self):
        return self.platforms

    # def detect_collision(self, player):
    #     for p in self.platforms:
    #         # TODO: check if platform is in range of camera view
    #         if player.player_rect.colliderect(p.get_rect()):
    #             # TODO
    #             print("Collision with platform detected!")