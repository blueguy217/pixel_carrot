import pygame

class Parallax():
    def __init__(self):
        self.layers = []
        imgs = ['Data/Img/l3.png', 'Data/Img/l2.png', 'Data/Img/l1.png']
        imgs = ['Data/Img/l3_blue.png', 'Data/Img/l2_blue.png', 'Data/Img/l1_blue.png']
        # imgs = ['Data/Img/l3_green.png', 'Data/Img/l2_green.png', 'Data/Img/l1_green.png']
        self.layers.append({'img': pygame.image.load(imgs[0]), 'speed': 0.2})
        self.layers.append({'img': pygame.image.load(imgs[1]), 'speed': 0.4})
        self.layers.append({'img': pygame.image.load(imgs[2]), 'speed': 0.6})

    def render(self, surface, scroll):
        bck_width = 320

        for l in self.layers:
            posx = int(-scroll[0] * l['speed'])
            posx_new = int(posx / bck_width) * bck_width - posx

            surface.blit(l['img'], (-posx_new, 0))
            surface.blit(l['img'], (-posx_new + bck_width, 0))
            surface.blit(l['img'], (-posx_new - bck_width, 0))

