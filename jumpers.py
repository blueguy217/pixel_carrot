import pygame
from tiledobject import *
from constants import *


class Jumper(TiledObject):
    def __init__(self, img, pos, rect):
        super().__init__(img, pos, rect, animated=False)
        self.push_force = 7
        self.current_frame = 0
        self.running_animation = False

    def action_on_collision(self, player, collision_types):
        player.player_y_momentum = -self.push_force
        self.running_animation = True

    def render(self, surface, scroll):
        if self.running_animation:
            self.frame_counter += 1
            if self.frame_counter > 0 and self.frame_counter < 5:   # going up
                self.current_frame = 1
            elif self.frame_counter > 0 and self.frame_counter < 10:    # top
                self.current_frame = 2
            elif self.frame_counter > 0 and self.frame_counter < 15:    # going down
                self.current_frame = 1
            else:
                self.frame_counter = 0
                self.current_frame = 0
                self.running_animation = False
            surface.blit(self.img[self.current_frame], (self.pos.x-scroll[0], self.pos.y-scroll[1]))
        else:
            super().render(surface, scroll)


class Jumpers():
    def __init__(self, jumpers):
        self.jumpers = []

        jumper_img = pygame.image.load('Data/Img/jumper.png')
        jumper_img1 = pygame.image.load('Data/Img/jumper_1.png')
        jumper_img2 = pygame.image.load('Data/Img/jumper_2.png')
        self.img = [jumper_img, jumper_img1, jumper_img2]

        for j in jumpers:
            pos = Position(j[0], j[1])
            collide_rect = pygame.Rect(j[0], j[1] + 10, TILE_SIZE, TILE_SIZE - 10)
            self.jumpers.append(Jumper(self.img, pos, collide_rect))

    def get_tiles(self):
        return self.jumpers


