import pygame

class Gui:
    def __init__(self, player):
        self.img_health_bar = pygame.image.load('Data/Img/health_bar.png')
        self.img_health_carrot = pygame.image.load('Data/Img/health_carrot.png')
        self.img_health_no_carrot = pygame.image.load('Data/Img/health_no_carrot.png')

        self.player = player

    def render(self, surface):
        surface.blit(self.img_health_bar, (0, 2))

        carrots_pos = [(4, 3), (20, 3), (36, 3)]
        if self.player.lives == 0:
            for i in carrots_pos:
                surface.blit(self.img_health_no_carrot, i)
        elif self.player.lives == 1:
            surface.blit(self.img_health_carrot, carrots_pos[0])
            surface.blit(self.img_health_no_carrot, carrots_pos[1])
            surface.blit(self.img_health_no_carrot, carrots_pos[2])
        elif self.player.lives == 2:
            surface.blit(self.img_health_carrot, carrots_pos[0])
            surface.blit(self.img_health_carrot, carrots_pos[1])
            surface.blit(self.img_health_no_carrot, carrots_pos[2])
        elif self.player.lives == 3:
            for i in carrots_pos:
                surface.blit(self.img_health_carrot, i)