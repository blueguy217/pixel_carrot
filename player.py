import pygame

from constants import *
from gamedata import GameData
from particles import RunningDust, JumpDust

class Animation():
    def __init__(self, filename, frames):
        self.img = pygame.image.load(filename)
        self.frames = frames
        self.frame_multiplier = 2
        self.total_frames = self.frames * self.frame_multiplier
        self.current_frame = 0
        self.animation = []

        width = self.img.get_width()
        height = self.img.get_height()
        width_one = int(width / frames)

        for i in range(frames):
            self.animation.append(self.img.subsurface((i*width_one, 0, width_one, height)))

    def get_next_frame(self):
        self.current_frame += 1
        if self.current_frame > self.total_frames - 1:
            self.current_frame = 0
        current_img = int(self.current_frame / self.frame_multiplier)
        return self.animation[current_img]

    def reset_animation(self):
        self.current_frame = 0


class Player:
    def __init__(self, start_position, game: GameData):
        self.game = game

        self.player_img = pygame.image.load('Data/Img/bunny.png')
        self.player_img_jump = pygame.image.load('Data/Img/bunny_jump_up.png')
        self.player_img_fall = pygame.image.load('Data/Img/bunny_jump_down.png')
        # self.player_img.set_colorkey((254, 254, 254))
        self.walk_animation = Animation('Data/Img/bunny_animation.png', 6)

        self.player_rect = pygame.Rect(start_position[0],
                                       start_position[1],
                                       self.player_img.get_width(),
                                       self.player_img.get_height())

        # player data
        self.moving_right = False
        self.moving_left = False
        self.direction = 'right'

        self.player_y_momentum = 0
        self.last_pos = pygame.Vector2(self.player_rect.x, self.player_rect.y)
        self.on_ground = False
        self.on_ramp = False
        self.last_standing_pos = start_position
        self.air_timer = 0
        self.jumping = False

        self.particle_counter = 0

        self.movement = [0, 0]

        self.lives = 2

    def update_movement(self):
        self.movement = [0, 0]

        if self.moving_right:
            self.movement[0] += 2
        if self.moving_left:
            self.movement[0] -= 2
        self.player_y_momentum += GRAVITY
        self.movement[1] += self.player_y_momentum
        if self.player_y_momentum > MAX_Y_MOMENTUM:
            self.player_y_momentum = MAX_Y_MOMENTUM

        if self.jumping:
            self.jump()

    def blit(self, surface, scroll):

        # check if on_ground
        if self.last_pos.y == self.player_rect.y and self.air_timer < MAX_AIR_TIME:
            self.on_ground = True
            if self.air_timer == 0:
                # for revive after die in water
                self.last_standing_pos = (self.player_rect.x, self.player_rect.y)
        else:
            self.on_ground = False
        # self.last_pos.y = self.player_rect.y

        # reset walk animation if standing on ramp
        if self.on_ramp and self.last_pos.x == self.player_rect.x:
            self.walk_animation.reset_animation()
        # self.last_pos.x = self.player_rect.x

        # update direction
        if self.moving_right:
            self.direction = 'right'
        elif self.moving_left:
            self.direction = 'left'

        if self.moving_right and (self.on_ground or self.on_ramp):
            surface.blit(self.walk_animation.get_next_frame(),
                         (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))
            self.create_dust()
        elif self.moving_left and (self.on_ground or self.on_ramp):
            right_img = self.walk_animation.get_next_frame()
            left_img = pygame.transform.flip(right_img, True, False)
            surface.blit(left_img,
                         (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))
            self.create_dust()

        elif self.direction == 'right' and self.on_ramp:   # idle on ramp
            surface.blit(self.walk_animation.get_next_frame(),
                         (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))
        elif self.direction == 'left' and self.on_ramp:
            right_img = self.walk_animation.get_next_frame()
            left_img = pygame.transform.flip(right_img, True, False)
            surface.blit(left_img,
                         (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))

        else:
            img = self.player_img
            if self.player_y_momentum > 1:  # falling
                img = self.player_img_fall
                self.walk_animation.reset_animation()
            if self.player_y_momentum < 0:  # jumping
                img = self.player_img_jump
                self.walk_animation.reset_animation()
            if self.direction == 'right':
                surface.blit(img, (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))
            else:
                surface.blit(pygame.transform.flip(img, True, False),
                             (self.player_rect.x - scroll[0], self.player_rect.y - scroll[1]))

        self.last_pos.x = self.player_rect.x
        self.last_pos.y = self.player_rect.y

        # DEBUG: draw player's last-standing-position
        # last_standing_rect = pygame.Rect(self.player_rect)
        # last_standing_rect.x = self.last_standing_pos[0] - scroll[0]
        # last_standing_rect.y = self.last_standing_pos[1] - scroll[1]
        # pygame.draw.rect(surface, (255, 0, 0), last_standing_rect)

    def jump(self):
        self.player_y_momentum -= JUMP_FORCE
        if self.player_y_momentum <= -MAX_JUMP_FORCE:
            self.player_y_momentum = -MAX_JUMP_FORCE
            self.jumping = False

    def start_jumping(self):
        self.create_jump_dust()
        self.jumping = True

    def life_up(self):
        self.lives += 1
        if self.lives > 3:
            self.lives = 3

    def create_dust(self, always=False):

        if not always:
            self.particle_counter += 1
            if self.particle_counter % 10 == 0:

                RunningDust(self.game)
                RunningDust(self.game)

            if self.particle_counter > 99:
                self.particle_counter = 0

    def create_jump_dust(self):
        JumpDust(self.game)
        JumpDust(self.game)
        JumpDust(self.game)