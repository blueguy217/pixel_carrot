
from gamedata import GameData
from constants import *

class Camera:
    def __init__(self, game: GameData):
        self.true_scroll = [0, 0]
        self.scroll = [0, 0]

        self.min_pos = (0, 0)
        # self.max_pos = ()

        self.game = game

    def update(self):
        diff_x = (self.game.player.player_rect.x - self.true_scroll[0] - int(
            DISPLAY_SIZE[0] / 2) + int(
            self.game.player.player_rect.w / 2))
        if diff_x > 6:
            self.true_scroll[0] += diff_x - 6
        elif diff_x < -6:
            self.true_scroll[0] += diff_x + 6
        # else:
        #     self.true_scroll[0] += diff_x / CAMERA_SPEED
        self.true_scroll[1] += (self.game.player.player_rect.y - self.true_scroll[1] - int(
            DISPLAY_SIZE[1] / 2) + int(
            self.game.player.player_rect.h / 2)) / CAMERA_SPEED
        self.true_scroll[1] -= CAMERA_Y_ADJ  # some adjustments

        if self.true_scroll[0] < self.min_pos[0]:
            self.true_scroll[0] = self.min_pos[0]
        if self.true_scroll[1] < self.min_pos[1]:
            self.true_scroll[1] = self.min_pos[1]

        self.scroll[0] = int(self.true_scroll[0])
        self.scroll[1] = int(self.true_scroll[1])

        # TODO: remove and use camera in map class
        self.game.map.scroll[0] = self.scroll[0]
        self.game.map.scroll[1] = self.scroll[1]
