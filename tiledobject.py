import pygame

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class TiledObject:
    def __init__(self, img, pos, rect, animated=False):
        self.img = img  # table of images
        self.pos = pos  # class Position
        self.rect = rect # collision rect

        self.active = True

        # animation
        self.animated = animated
        self.animation_speed = 20
        self.current_frame = 0
        self.frame_counter = 0

        self.collision_types = {'left': False, 'right': False, 'top': False, 'bottom': False}
        self.has_side_collisions = True

    def draw_collision_rect(self, surface, scroll):
        rect = pygame.Rect(self.rect.x - scroll[0], self.rect.y - scroll[1],
                           self.rect.w, self.rect.h)
        pygame.draw.rect(surface, (255,0,0), rect, 1)

    def update_frame(self):
        self.frame_counter += 1
        if self.frame_counter % self.animation_speed == 0:
            self.current_frame += 1
        if self.current_frame >= len(self.img):
            self.current_frame = 0
            self.frame_counter = 0

    def render(self, surface, scroll):
        if not self.active:
            return

        if self.animated:
            self.update_frame()
        # TODO: check if in range of camera
        surface.blit(self.img[self.current_frame], (self.pos.x - scroll[0], self.pos.y - scroll[1]))

    def get_collision_rect(self):
        if not self.active:
            return None

        # reset collisions
        self.collision_types = {'left': False, 'right': False, 'top': False, 'bottom': False}

        return self.rect

    def action_on_collision(self, player, collision_types):
        if not self.active:
            return

        self.collision_types = collision_types

        if collision_types['right'] and self.has_side_collisions:
            player.player_rect.right = self.rect.left
        elif collision_types['left'] and self.has_side_collisions:
            player.player_rect.left = self.rect.right
        elif collision_types['bottom']:
            player.player_rect.bottom = self.rect.top
        elif collision_types['top']:
            player.player_rect.top = self.rect.bottom
            player.jumping = False

        if collision_types['top'] or collision_types['bottom']:
            player.player_y_momentum = 0
