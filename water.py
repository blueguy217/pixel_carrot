import pygame

from tiledobject import TiledObject, Position
from constants import *


TILE_LEFT = 'left'
TILE_MIDDLE = 'middle'
TILE_RIGHT = 'right'

class WaterTile(TiledObject):
    def __init__(self, img, pos, collision_rect, type):
        super().__init__(img, pos, collision_rect, animated=True)

        self.type = type

    def action_on_collision(self, player, collision_types):
        # super().action_on_collision(player, collision_types)
        if collision_types['bottom'] and self.type == 'middle':
            print("Player is drowning!")
            player.player_rect.x = player.last_standing_pos[0]
            player.player_rect.y = player.last_standing_pos[1]
        else:
            super().action_on_collision(player, collision_types)

class Water:
    def __init__(self, water_areas):
        self.img = pygame.image.load('Data/Img/water_ground.png')
        self.img_left = [self.img.subsurface((0 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE)),
                         self.img.subsurface((0 * TILE_SIZE, 1 * TILE_SIZE, TILE_SIZE, TILE_SIZE))]
        self.img_middle = [self.img.subsurface((1 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE)),
                           self.img.subsurface((1 * TILE_SIZE, 1 * TILE_SIZE, TILE_SIZE, TILE_SIZE))]
        self.img_right = [self.img.subsurface((2 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE)),
                          self.img.subsurface((2 * TILE_SIZE, 1 * TILE_SIZE, TILE_SIZE, TILE_SIZE))]

        self.tiles = []
        for i in water_areas:
            width = int(i.w/16)
            if width < 3:
                print("ERROR! Water area is too short")

            # left
            position = Position(i.x, i.y)
            collision_rect = pygame.Rect(i.x, i.y, TILE_SIZE, TILE_SIZE)
            self.tiles.append(WaterTile(self.img_left, position, collision_rect, TILE_LEFT))

            # right
            position = Position(i.x + i.w - TILE_SIZE, i.y)
            collision_rect = pygame.Rect(position.x, position.y, TILE_SIZE, TILE_SIZE)
            self.tiles.append(WaterTile(self.img_right, position, collision_rect, TILE_RIGHT))

            # middle
            for j in range(1, width-1):
                position = Position(i.x + j * TILE_SIZE, i.y)
                collision_rect = pygame.Rect(position.x, position.y+7, TILE_SIZE, TILE_SIZE-7)
                self.tiles.append(WaterTile(self.img_middle, position, collision_rect, TILE_MIDDLE))

    def get_tiles(self):
        return self.tiles
